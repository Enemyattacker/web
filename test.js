﻿describe("сложение", function(){
	
		
	function makeTest(x,y){
		var excepted = x+y;
			it("при сложении " + x + " и  " + y + " получается " + excepted, function(){
				assert.equal(add(x,1),excepted);
			});
	}
	
	for(var i= -1.5; i<= 1.5; i= i + 0.3){
		makeTest(i,1);
	}
	
	
});

describe ("вычитание", function(){
	function makeTest(x, y){
		var excepted = x-y;
		it("при вычитании " + x + " и " + y + " получается " + excepted, function(){
			assert.equal(substr(x,1),excepted);
		});
	}
	
	for(var i= -1.5; i<= 1.5; i=i + 0.3){
		makeTest(i,1);
	}
	
	
});

describe ("умножение", function(){
	function makeTest(x, y){
		var excepted = x*y;
		it("при умножении " + x + " и " + y + " получается " + excepted, function(){
			assert.equal(multply(x,2),excepted);
		});
	}
	
	for(var i= -1.5; i<= 1.5; i=i + 0.3){
		makeTest(i,2);
	}
	
	
});

describe ("деление", function(){
	function makeTest(x, y){
		var excepted = x/y;
		it("при делении " + x + " и " + y + " получается " + excepted, function(){
			assert.equal(divide(2,y),excepted);
		});
	}
	
	for(var i= -1.5; i<= 1.5; i=i + 0.5){
		makeTest(2,i);
	}
	
	it("деление нуля на себя даёт NaN", function() {
    assert(isNaN(divide(0, 0)), "0/0 != NaN");
  });
	
});

describe ("факториал", function(){

		it("факториал 5 получается 120", function(){
			assert.equal(factorial(5),120);
		});
		
		it("факториал отрицательного числа даёт NaN", function(){
			assert(isNaN(factorial(-1)));
		});
		
		it("факториал 0 равен 1", function(){
			assert.equal(factorial(0),1);
		});
		
		it("факториал дробного числа даёт NaN", function(){
			assert(isNaN(factorial(2.5)));
		});
	
});





